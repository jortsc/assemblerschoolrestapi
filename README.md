Laminas API Tools Skeleton Application ha sido utilzada en este ejemplo práctico
==============================

Instalación
------------

### Via Git (clone)

Primero, clonar el repo!

```bash
# git clone https://gitlab.com/jortsc/dddbyassemblerschool.git #optionally, specify the directory in which to clone
$ cd path/to/install
```
En este punto necesitaras [Composer](https://getcomposer.org/) 
para la instalación de las dependencias. Asumiendo que ya lo tienes listo:

```bash
$ composer install
```

### All methods

Una vez instalado, vamos a activar el modo desarrollo del framework:

```bash
$ cd path/to/install
$ composer development-enable
```

Para ejecutarlo vamos a utilizar el servidor nativo de PHP

```bash
$ cd path/to/install
$ php -S 0.0.0.0:8080 -ddisplay_errors=0 -t public public/index.php
# OR use the composer alias:
$ composer serve
```

Ahora puedes visitar http://localhost:8080/ - en el cual veras el panel de control del framework


QA Tools
--------
Tenemos integrado para la calidad del código a PHP Code Sniffer [phpcs](https://github.com/squizlabs/php_codesniffer).

En Composer tenemos algunos alias para ejecutar los test del Framework y el Code Sniffer:

```bash
# Run CS checks:
$ composer cs-check
# Fix CS errors:
$ composer cs-fix
# Run PHPUnit tests:
$ composer test
```

DDD Application Tests
--------

Para ejecutar de forma aisla nuestros tests y poder jugar con ellos sin involucrar todos los tests podemos configurar
nuestro ide para que ejecute los tests de app/tests o simplemente podemos ejecutar el sigiuente comando:

```bash
php vendor/phpunit/phpunit/phpunit --debug --verbose --colors -c app/tests/phpunit.xml  --stop-on-failure app/tests/
```

Definición del proyecto
--------
Utilizando el Framework Apigility o Laminas, vamos a montar un endpoint rest `/user/contact/contact_id]` el cual va a 
explotar un servicio de nuestra aplicación hexagonal llamado `AddContactService` el cual pretende almacenar contactos 
de un usuario de nuestro sistema.
Para ello nos pasan tres casos de usuario:
1. Como Mike, cuando un contacto de un usuario es guardado quiero que se almacenen las coordenadas de la localización 
para su posterior detección y mejora de experiencia en dispositivos móviles
2. Como Jose quiero que cuando un contacto sea almacenado se dispare un nuevo evento llamado: `ContactCreatedEvent` y que 
se dispare la integración de contactos contra Zapier.
3. Como Mike, quiero que se calcule la reputación del contacto cuando es creado.

Lo abordaremos en dos partes:

1. Creación de toda la estructura hexagonal y la implementación del nuevo servicio dentro de la misma
2. Añadir el recurso o endpoint y realizar la llamada al servicio desde el controlador.

La estructura de la aplicación basada en arquitectura hexagonal como hablemos se encuentra en:

```
    app/
    |----Application/
    |----Domain/
    |----Infrastructure/
    |----tests/ 

```

El resto es el propio framework del API REST. 

Como ya se comentó podemos organizar estos mismo directorios de forma 
distinta por ejemplo situandolos en la raíz pero se mezcla con directorios del framework y hace mas difícil 
la comprensión de la arquitectura.

Code Sniffer Check:

```
~/projects/AssemblerSchoolApi ᐅ composer cs-check
> phpcs
....W............W..EE.... 26 / 26 (100%)


FILE: ...pi/config/autoload/api-tools-mvc-auth-oauth2-override.global.php
----------------------------------------------------------------------
FOUND 0 ERRORS AND 1 WARNING AFFECTING 1 LINE
----------------------------------------------------------------------
 11 | WARNING | Line exceeds 120 characters; contains 134 characters
----------------------------------------------------------------------


FILE: ...s/AssemblerSchoolApi/module/Application/config/module.config.php
----------------------------------------------------------------------
FOUND 0 ERRORS AND 1 WARNING AFFECTING 1 LINE
----------------------------------------------------------------------
 31 | WARNING | Line exceeds 120 characters; contains 148 characters
----------------------------------------------------------------------

```

Code Sniffer Fix:

```
~/projects/AssemblerSchoolApi ᐅ composer cs-fix  
> phpcbf
....................FF.... 26 / 26 (100%)


PHPCBF RESULT SUMMARY
----------------------------------------------------------------------
FILE                                                  FIXED  REMAINING
----------------------------------------------------------------------
...SchoolApi/module/Application/view/error/404.phtml  4      0
...hoolApi/module/Application/view/error/index.phtml  2      0
----------------------------------------------------------------------
A TOTAL OF 6 ERRORS WERE FIXED IN 2 FILES
----------------------------------------------------------------------

Time: 541ms; Memory: 8MB

```
Framework tests:

```
~/projects/AssemblerSchoolApi ᐅ composer test                                   
> phpunit
PHPUnit 9.1.4 by Sebastian Bergmann and contributors.

....                                                                4 / 4 (100%)

Time: 00:00.337, Memory: 14.00 MB

OK (4 tests, 9 assertions)

```

App tests:

```
~/projects/AssemblerSchoolApi (master ✘)✭ ᐅ php vendor/phpunit/phpunit/phpunit --debug --verbose --colors -c app/tests/phpunit.xml  --stop-on-failure app/tests/
PHPUnit 9.1.4 by Sebastian Bergmann and contributors.

Runtime:       PHP 7.3.7 with Xdebug 2.7.2
Configuration: app/tests/phpunit.xml

Test 'DDDByAssemblerSchool\tests\Application\Service\AddContactServiceTest::testCanBeCreated' started
Test 'DDDByAssemblerSchool\tests\Application\Service\AddContactServiceTest::testCanBeCreated' ended
Test 'DDDByAssemblerSchool\tests\Application\Service\AddContactServiceTest::testShouldAddCoordinates' started
Test 'DDDByAssemblerSchool\tests\Application\Service\AddContactServiceTest::testShouldAddCoordinates' ended
Test 'DDDByAssemblerSchool\tests\Application\Service\AddContactServiceTest::testShouldAddReputation' started
Test 'DDDByAssemblerSchool\tests\Application\Service\AddContactServiceTest::testShouldAddReputation' ended
Test 'DDDByAssemblerSchool\tests\Domain\AddContactTest::testCanBeCreated' started
Test 'DDDByAssemblerSchool\tests\Domain\AddContactTest::testCanBeCreated' ended
Test 'DDDByAssemblerSchool\tests\Domain\AddContactTest::testShouldAddCoordinates' started
Test 'DDDByAssemblerSchool\tests\Domain\AddContactTest::testShouldAddCoordinates' ended
Test 'DDDByAssemblerSchool\tests\Domain\AddContactTest::testShouldAddReputation' started
Test 'DDDByAssemblerSchool\tests\Domain\AddContactTest::testShouldAddReputation' ended
Test 'DDDByAssemblerSchool\tests\Domain\ContactTest::testCanBeCreated' started
Test 'DDDByAssemblerSchool\tests\Domain\ContactTest::testCanBeCreated' ended
Test 'DDDByAssemblerSchool\tests\Domain\ContactTest::testShouldBecomeAnArrayWithPublicProperties' started
Test 'DDDByAssemblerSchool\tests\Domain\ContactTest::testShouldBecomeAnArrayWithPublicProperties' ended
Test 'DDDByAssemblerSchool\tests\Domain\EventPublisherTest::testShouldNotifySubscriber' started
Test 'DDDByAssemblerSchool\tests\Domain\EventPublisherTest::testShouldNotifySubscriber' ended
Test 'DDDByAssemblerSchool\tests\Domain\EventPublisherTest::testShouldNotifyFromCreatedContact' started
Test 'DDDByAssemblerSchool\tests\Domain\EventPublisherTest::testShouldNotifyFromCreatedContact' ended
Test 'DDDByAssemblerSchool\tests\Infrastructure\Persistence\Repository\InMemoryContactRepositoryTest::testShouldBeCreated' started
Test 'DDDByAssemblerSchool\tests\Infrastructure\Persistence\Repository\InMemoryContactRepositoryTest::testShouldBeCreated' ended
Test 'DDDByAssemblerSchool\tests\Infrastructure\Persistence\Repository\InMemoryContactRepositoryTest::testShouldSaveAContact' started
Test 'DDDByAssemblerSchool\tests\Infrastructure\Persistence\Repository\InMemoryContactRepositoryTest::testShouldSaveAContact' ended


Time: 00:00.128, Memory: 6.00 MB

OK (12 tests, 13 assertions)
```

Todo junto
--------

```
~/projects/AssemblerSchoolApi (master ✘)✭ ᐅ curl -vs 
--data '{"name":"Jose", "email":"jortsc@gmail.com", "street":"Somewhere", "number":"8", "city":"Valencia", "country":"Spain", "postalCode":"12345"}'  
-H "Content-Type: application/json" http://0.0.0.0:8080/contact


{
  "name": "Jose",
  "email": "jortsc@gmail.com",
  "street": "Somewhere",
  "city": "Valencia",
  "country": "Spain",
  "postalCode": "12345",
  "_links": {
    "self": {
      "href": "http:\/\/0.0.0.0:8080\/contact"
    }
  }
}
```
