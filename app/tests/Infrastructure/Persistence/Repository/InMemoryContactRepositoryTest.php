<?php
namespace DDDByAssemblerSchool\tests\Infrastructure\Persistence\Repository;

use DDDByAssemblerSchool\Application\Service\AddContactServiceRequest;
use DDDByAssemblerSchool\Domain\Contact;
use DDDByAssemblerSchool\Domain\Repository\Exception\ContactNotFoundException;
use DDDByAssemblerSchool\Infrastructure\Persistence\Repository\InMemoryContactRepository;
use PHPUnit\Framework\TestCase;

/**
 * @author: Jose Manuel Orts
 * @date: 09/05/2020
 */
class InMemoryContactRepositoryTest extends TestCase
{

    public function testShouldBeCreated()
    {
        static::assertInstanceOf(
            InMemoryContactRepository::class,
            new InMemoryContactRepository()
        );
    }

    public function testShouldSaveAContact()
    {
        $request = new AddContactServiceRequest(
            'Jose',
            'jortsc@gmail.com',
            'Somewhere',
            '8',
            'Valencia',
            'Spain',
            '12345'
        );

        $contact = (new InMemoryContactRepository())
            ->create(Contact::createFromRequest($request));

        static::assertEquals(
            Contact::createFromRequest($request),
            $contact->setId(null)
        );
    }

    public function testShouldThrowAnExceptionWhenContactNoFound()
    {
        $this->expectException(ContactNotFoundException::class);
        $this->expectExceptionMessage('Not found');

        (new InMemoryContactRepository())->findById('nonExistentOne');
    }

    public function testShouldGetGivenContact()
    {
        $request = new AddContactServiceRequest(
            'Jose',
            'jortsc@gmail.com',
            'Somewhere',
            '8',
            'Valencia',
            'Spain',
            '12345'
        );

        $repository = new InMemoryContactRepository();
        $createdContact = $repository->create(Contact::createFromRequest($request));

        $foundContact = $repository->findById($createdContact->getId());

        static::assertEquals($createdContact, $foundContact);
    }
}
