<?php
namespace DDDByAssemblerSchool\tests\Application\Service;

use DDDByAssemblerSchool\Application\Service\AddContactService;
use DDDByAssemblerSchool\Application\Service\AddContactServiceRequest;
use DDDByAssemblerSchool\Domain\Contact;
use DDDByAssemblerSchool\Domain\Event\Publisher;
use DDDByAssemblerSchool\Infrastructure\GoogleCoordinates;
use DDDByAssemblerSchool\Infrastructure\Persistence\Repository\InMemoryContactRepository;
use DDDByAssemblerSchool\Infrastructure\ReputationApi;
use DDDByAssemblerSchool\Infrastructure\Subscribers\ZapierSubscriber;
use PHPUnit\Framework\TestCase;

/**
 * @author: Jose Manuel Orts
 * @date: 09/05/2020
 */

class AddContactServiceTest extends TestCase
{

    public function testCanBeCreated()
    {
        $repository = new InMemoryContactRepository();
        $reputation = new ReputationApi();
        $coordinates = new GoogleCoordinates();

        static::assertInstanceOf(
            AddContactService::class,
            new AddContactService($repository, $coordinates, $reputation)
        );
    }


    /**
     * As Mike we want to get and save coordinates from new contacts in order to geolocate them
    */
    public function testShouldAddCoordinates()
    {
        $contactService = $this->getContactService();
        $request = $this->getContactRequest();
        $expectedContact = Contact::createFromRequest($request);
        $expectedContact->setCoordinates('41.40338, 2.17403');

        $contact = $contactService->execute($request);

        static::assertEquals($expectedContact->getCoordinates(), $contact->getCoordinates());

    }

    /**
     * As Jose we want to run Zapier integration when a contact is created
    */
    public function testShouldNotifyFromCreatedContact()
    {
        $contactService = $this->getContactService();
        $request = $this->getContactRequest();

        $subscriber = new ZapierSubscriber();
        Publisher::instance()->subscribe($subscriber);

        $contactService->execute($request);

        self::assertTrue($subscriber->isHandled);
    }

    /**
     * As Mike we want to call reputation api in order to get reputation and save it
     * in order to be able to segment them once is created
     */
    public function testShouldAddReputation()
    {
        $contactService = $this->getContactService();
        $request = $this->getContactRequest();
        $expectedContact = Contact::createFromRequest($request);
        $expectedContact->setReputation(48);

        $contact = $contactService->execute($request);

        static::assertEquals($expectedContact->getReputation(), $contact->getReputation());

    }



    /**
     * @return AddContactService
     */
    private function getContactService()
    {
        $repository = new InMemoryContactRepository();
        $reputation = new ReputationApi();
        $coordinates = new GoogleCoordinates();

        return new AddContactService($repository, $coordinates, $reputation);
    }

    /**
     * @return AddContactServiceRequest
     */
    private function getContactRequest()
    {
        $request = new AddContactServiceRequest(
            'Jose',
            'jortsc@gmail.com',
            'Somewhere',
            '8',
            'Valencia',
            'Spain',
            '12345'
        );

        return $request;
    }

}
