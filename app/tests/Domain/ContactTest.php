<?php
namespace DDDByAssemblerSchool\tests\Domain;

use DDDByAssemblerSchool\Application\Service\AddContactServiceRequest;
use DDDByAssemblerSchool\Domain\Contact;
use PHPUnit\Framework\TestCase;

/**
 * @author: Jose Manuel Orts
 * @date: 09/05/2020
 */
class ContactTest extends TestCase
{
    public function testCanBeCreated()
    {
        static::assertInstanceOf(
            Contact::class,
            Contact::createFromRequest($this->getContactRequest())
        );
    }

    /**
     * As Mike we want to get and save coordinates from new contacts in order to geolocate them
    */
    public function testShouldBecomeAnArrayWithPublicProperties()
    {
        $contact = Contact::createFromRequest($this->getContactRequest());
        $keys = array_keys($contact->toArray());

        $publicProperties = ['id', 'name', 'email', 'street', 'city', 'country', 'postalCode'];

        static::assertEquals($publicProperties, $keys);
    }


    /**
     * @return AddContactServiceRequest
     */
    private function getContactRequest()
    {
        $request = new AddContactServiceRequest(
            'Jose',
            'jortsc@gmail.com',
            'Somewhere',
            '8',
            'Valencia',
            'Spain',
            '12345'
        );

        return $request;
    }
}
