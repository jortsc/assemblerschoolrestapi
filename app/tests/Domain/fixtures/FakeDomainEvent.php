<?php
namespace DDDByAssemblerSchool\tests\Domain\fixtures;

use DDDByAssemblerSchool\Domain\Event\Event;

/**
 * @author: Jose Manuel Orts
 * @date: 09/05/2020
 */

class FakeDomainEvent implements Event
{
    public function __construct()
    {
    }

    public function occurredOn(): string
    {
        return '';
    }

    public function getEventObject(): Contact
    {
        return ;
    }
}
