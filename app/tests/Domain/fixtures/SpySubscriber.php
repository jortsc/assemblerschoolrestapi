<?php
namespace DDDByAssemblerSchool\tests\Domain\fixtures;

use DDDByAssemblerSchool\Domain\Event\Event;
use DDDByAssemblerSchool\Domain\Event\Subscriber;

/**
 * @author: Jose Manuel Orts
 * @date: 09/05/2020
 */
class SpySubscriber implements Subscriber
{
    public $domainEvent;
    public $isHandled = false;
    private $eventName;

    public function __construct($eventName)
    {
        $this->eventName = $eventName;
    }

    public function isSubscribedTo(Event $aDomainEvent): bool
    {
        return $this->eventName === get_class($aDomainEvent);
    }

    public function handle(Event $aDomainEvent): bool
    {
        $this->domainEvent = $aDomainEvent;
        $this->isHandled = true;

        return $this->isHandled;
    }
}
