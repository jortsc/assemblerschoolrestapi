<?php
namespace DDDByAssemblerSchool\tests\Domain;

use DDDByAssemblerSchool\Application\Service\AddContactServiceRequest;
use DDDByAssemblerSchool\Domain\AddContact;
use DDDByAssemblerSchool\Domain\Contact;
use DDDByAssemblerSchool\Infrastructure\GoogleCoordinates;
use DDDByAssemblerSchool\Infrastructure\ReputationApi;
use PHPUnit\Framework\TestCase;

/**
 * @author: Jose Manuel Orts
 * @date: 09/05/2020
 */

class AddContactTest extends TestCase
{

    public function testCanBeCreated()
    {
        static::assertInstanceOf(
            AddContact::class,
            new AddContact()
        );
    }


    /**
     * As Mike we want to get and save coordinates from new contacts in order to geolocate them
    */
    public function testShouldAddCoordinates()
    {
        $addContact = new AddContact();

        $contact = $addContact->process($this->getContactRequest(), new GoogleCoordinates(), new ReputationApi());

        $request = $this->getContactRequest();
        $expectedContact = Contact::createFromRequest($request);
        $expectedContact->setCoordinates('41.40338, 2.17403');

        static::assertEquals($expectedContact->getCoordinates(), $contact->getCoordinates());

    }

    /**
     * As Mike we want to call reputation api in order to get reputation and save it
     * in order to be able to segment them once is created
     */
    public function testShouldAddReputation()
    {
        $addContact = new AddContact();

        $contact = $addContact->process($this->getContactRequest(), new GoogleCoordinates(), new ReputationApi());

        $request = $this->getContactRequest();
        $expectedContact = Contact::createFromRequest($request);
        $expectedContact->setReputation(48);

        static::assertEquals($expectedContact->getReputation(), $contact->getReputation());

    }



    /**
     * @return AddContactServiceRequest
     */
    private function getContactRequest()
    {
        $request = new AddContactServiceRequest(
            'Jose',
            'jortsc@gmail.com',
            'Somewhere',
            '8',
            'Valencia',
            'Spain',
            '12345'
        );

        return $request;
    }
}
