<?php
namespace DDDByAssemblerSchool\tests\Domain;

use DDDByAssemblerSchool\Application\Service\AddContactServiceRequest;
use DDDByAssemblerSchool\Domain\Contact;
use DDDByAssemblerSchool\Domain\Event\ContactCreatedEvent;
use DDDByAssemblerSchool\Domain\Event\Publisher;
use DDDByAssemblerSchool\tests\Domain\fixtures\SpySubscriber;
use PHPUnit\Framework\TestCase;

/**
 * @author: Jose Manuel Orts
 * @date: 09/05/2020
 */
class EventPublisherTest extends TestCase
{
    public function testShouldNotifySubscriber()
    {
        $request = new AddContactServiceRequest(
            'Jose',
            'jortsc@gmail.com',
            'Somewhere',
            '8',
            'Valencia',
            'Spain',
            '12345'
        );
        $contact = Contact::createFromRequest($request);

        $subscriber = new SpySubscriber(ContactCreatedEvent::class);
        Publisher::instance()->subscribe($subscriber);

        $domainEvent = new ContactCreatedEvent($contact);
        Publisher::instance()->publish($domainEvent);

        self::assertTrue($subscriber->isHandled);
        self::assertEquals($domainEvent, $subscriber->domainEvent);
    }

}
