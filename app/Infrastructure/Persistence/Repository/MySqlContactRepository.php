<?php
namespace DDDByAssemblerSchool\Infrastructure\Persistence\Repository;

use DDDByAssemblerSchool\Domain\Contact;
use DDDByAssemblerSchool\Domain\Repository\ContactRepository;

/**
 * @author: Jose Manuel Orts
 * @date: 09/05/2020
 */
class MySqlContactRepository implements ContactRepository
{
    /**
     * TODO
     * this is just to show that we can create many adapters
     * for this port and instance the application based on the entry point or client
     * So powerful
     */


    public function create(Contact $contact): Contact
    {
        // TODO: Implement create() method.
    }

    public function findById($id)
    {
        // TODO: Implement findById() method.
    }
}
