<?php
namespace DDDByAssemblerSchool\Infrastructure\Persistence\Repository;

use DDDByAssemblerSchool\Domain\Contact;
use DDDByAssemblerSchool\Domain\Repository\ContactRepository;
use DDDByAssemblerSchool\Domain\Repository\Exception\ContactNotFoundException;

/**
 * @author: Jose Manuel Orts
 * @date: 09/05/2020
 */
class InMemoryContactRepository implements ContactRepository
{
    private $storage = [];

    public function create(Contact $contact): Contact
    {
        $contact->setId(uniqid());
        $this->storage[$contact->getId()] = $contact;

        return $contact;
    }

    public function findById($id)
    {
        if (!isset($this->storage[$id])) {
            throw new ContactNotFoundException('Not found');
        }
        return $this->storage[$id];
    }
}
