<?php
namespace DDDByAssemblerSchool\Infrastructure;

use DDDByAssemblerSchool\Domain\Coordinates;

/**
 * @author: Jose Manuel Orts
 * @date: 09/05/2020
 */

class GoogleCoordinates implements Coordinates
{

    public function get($street, $postalCode, $city, $country): string
    {
        return '41.40338, 2.17403';
    }
}
