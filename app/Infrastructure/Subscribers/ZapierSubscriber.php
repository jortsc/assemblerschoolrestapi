<?php
namespace DDDByAssemblerSchool\Infrastructure\Subscribers;

use DDDByAssemblerSchool\Domain\Event\ContactCreatedEvent;
use DDDByAssemblerSchool\Domain\Event\Event;
use DDDByAssemblerSchool\Domain\Event\Subscriber;

/**
 * @author: Jose Manuel Orts
 * @date: 09/05/2020
 */
class ZapierSubscriber implements Subscriber
{
    private $eventName = ContactCreatedEvent::class;
    public $isHandled = false;
    public $domainEvent;


    /**
     * @param ContactCreatedEvent
     */
    public function handle(Event $domainEvent): bool
    {
        //Get Contact and make amazing things or just pass it to Zapier
        $domainEvent->getEventObject();

        //Fancy call to Zapier api
        $this->isHandled = true;
        $this->domainEvent = $domainEvent;
        return true;
    }

    /**
     * @inheritDoc
     */
    public function isSubscribedTo(Event $domainEvent): bool
    {
        return $this->eventName === get_class($domainEvent);
    }
}
