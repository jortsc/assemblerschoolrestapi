<?php
namespace DDDByAssemblerSchool\Application\Service;

/**
 * @author: Jose Manuel Orts
 * @date: 09/05/2020
 */
class AddContactServiceRequest
{
    public $name;
    public  $email;
    public  $street;
    public  $number;
    public  $city;
    public  $country;
    public $postalCode;

    /**
     * AddContactServiceRequest constructor.
     * @param String $name
     * @param String  $email
     * @param String $street
     * @param int $number
     * @param String $city
     * @param String $country
     * @param int $postalCode
     */
    public function __construct($name, $email, $street, $number, $city, $country, $postalCode)
    {
        $this->name = $name;
        $this->email = $email;
        $this->street = $street;
        $this->number = $number;
        $this->city = $city;
        $this->country = $country;
        $this->postalCode = $postalCode;
    }
}
