<?php
namespace DDDByAssemblerSchool\Application\Service;

use DDDByAssemblerSchool\Domain\AddContact;
use DDDByAssemblerSchool\Domain\Contact;
use DDDByAssemblerSchool\Domain\Coordinates;
use DDDByAssemblerSchool\Domain\Event\ContactCreatedEvent;
use DDDByAssemblerSchool\Domain\Event\Publisher;
use DDDByAssemblerSchool\Domain\Repository\ContactRepository;
use DDDByAssemblerSchool\Domain\Reputation;

/**
 * @author: Jose Manuel Orts
 * @date: 09/05/2020
 */
class AddContactService
{
    /**
     * @var ContactRepository
     */
    private $repository;
    /**
     * @var Coordinates
     */
    private $coordinates;
    /**
     * @var Reputation
     */
    private $reputation;

    /**
     * ProcessTemplateService constructor.
     * @param ContactRepository $repository
     */
    public function __construct(ContactRepository $repository, Coordinates $coordinates, Reputation $reputation)
    {
        $this->repository = $repository;
        $this->coordinates = $coordinates;
        $this->reputation = $reputation;
    }

    /**
     * @param AddContactServiceRequest $request
     * @return Contact
     */
    public function execute(AddContactServiceRequest $request): Contact
    {
        $contact = (new AddContact())
            ->process($request, $this->coordinates, $this->reputation);

        $contact = $this->repository->create($contact);
        Publisher::instance()->publish(new ContactCreatedEvent($contact));

        return $contact;
    }
}
