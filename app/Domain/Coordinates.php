<?php
namespace DDDByAssemblerSchool\Domain;

/**
 * @author: Jose Manuel Orts
 * @date: 09/05/2020
 */
interface Coordinates
{
    /**
     * @param  String $street
     * @param  int $postalCode
     * @param  String $city
     * @param  String $country
     *
     * @return bool
     */
    public function get($street, $postalCode, $city, $country): string;
}
