<?php
namespace DDDByAssemblerSchool\Domain\Repository\Exception;

/**
 * @author: Jose Manuel Orts
 * @date: 09/05/2020
 */
class ContactNotFoundException extends \Exception {}
