<?php
namespace  DDDByAssemblerSchool\Domain\Repository;

use DDDByAssemblerSchool\Domain\Contact;
use DDDByAssemblerSchool\Domain\Repository\Exception\ContactNotFoundException;

/**
 * @author: Jose Manuel Orts
 * @date: 09/05/2020
 */
interface ContactRepository
{
    /**
     * @param Contact $contact
     * @return Contact
     */
    public function create(Contact $contact): Contact;

    /**
     * @param String $id
     * @return Contact
     * @throws ContactNotFoundException
     */
    public function findById($id);
}
