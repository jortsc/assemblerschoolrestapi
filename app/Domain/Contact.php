<?php
namespace DDDByAssemblerSchool\Domain;

use DDDByAssemblerSchool\Application\Service\AddContactServiceRequest;

/**
 * Data Object
 *
 * @author: Jose Manuel Orts
 * @date: 09/05/2020
 */
class Contact
{
    private $id;
    private $name;
    private $email;
    private $street;
    private $number;
    private $city;
    private $country;
    private $postalCode;
    private $reputation;
    private $coordinates;

    private function __construct($name, $email, $street, $number, $city, $country, $postalCode)
    {
        $this->name = $name;
        $this->email = $email;
        $this->street = $street;
        $this->number = $number;
        $this->city = $city;
        $this->country = $country;
        $this->postalCode = $postalCode;
    }

    /**
     * Contact constructor.
     * @param $name
     * @param $email
     * @param $street
     * @param $number
     * @param $city
     * @param $country
     * @param $postalCode
     */

    public static function createFromRequest(AddContactServiceRequest $request): Contact
    {
        return new self(
            $request->name,
            $request->email,
            $request->street,
            $request->number,
            $request->city,
            $request->country,
            $request->postalCode
        );
    }

    public function toArray(): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'email' => $this->email,
            'street' => $this->street,
            'city' => $this->city,
            'country' => $this->country,
            'postalCode' => $this->postalCode
        ];
    }

    public function getName()
    {
        return $this->name;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function getStreet()
    {
        return $this->street;
    }

    public function getNumber()
    {
        return $this->number;
    }

    public function getCity()
    {
        return $this->city;
    }

    public function getCountry()
    {
        return $this->country;
    }

    public function getPostalCode()
    {
        return $this->postalCode;
    }

    public function getReputation()
    {
        return $this->reputation;
    }

    public function setReputation($reputation)
    {
        $this->reputation = $reputation;

        return $this;
    }

    public function getCoordinates()
    {
        return $this->coordinates;
    }

    public function setCoordinates($coordinates)
    {
        $this->coordinates = $coordinates;

        return $this;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }
}
