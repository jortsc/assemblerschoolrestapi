<?php
namespace DDDByAssemblerSchool\Domain\Event;

use DDDByAssemblerSchool\Domain\Contact;

/**
 * @author: Jose Manuel Orts
 * @date: 09/05/2020
 */
Class ContactCreatedEvent implements Event
{
    /**
     * @var Contact
     */
    private $contact;

    /**
     * ContactCreatedEvent constructor.
     */
    public function __construct(Contact $contact)
    {
        $this->contact = $contact;
    }

    /**
     * @return string
     */
    public function occurredOn(): string
    {
        return (new \DateTime())->format('Y-m-d H:i:s');
    }

    /**
     * @return  Contact
     */
    public function getEventObject(): Contact
    {
        return $this->contact;
    }
}
