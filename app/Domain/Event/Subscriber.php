<?php
namespace DDDByAssemblerSchool\Domain\Event;

/**
 * @author: Jose Manuel Orts
 * @date: 09/05/2020
 */
interface Subscriber
{
    /**
     * @param Event $aDomainEvent
     * @return bool
     */
    public function handle(Event $aDomainEvent): bool;
    /**
     * @param Event $aDomainEvent
     * @return bool
     */
    public function isSubscribedTo(Event $aDomainEvent): bool;
}
