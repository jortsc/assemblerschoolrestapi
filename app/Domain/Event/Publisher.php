<?php
namespace DDDByAssemblerSchool\Domain\Event;

/**
 * @author: Jose Manuel Orts
 * @date: 09/05/2020
 */
class Publisher
{
    /**
     * @var Subscriber[]
     */
    private $subscribers;
    /**
     * @var Publisher
     */
    private static $instance = null;
    private $id = 1;

    public static function instance(): Publisher
    {
        if (null === static::$instance) {
            static::$instance = new self();
        }

        return static::$instance;
    }

    private function __construct()
    {
        $this->subscribers = [];
    }

    public function __clone()
    {
        throw new \BadMethodCallException('Clone is not supported');
    }

    public function subscribe($aDomainEventSubscriber)
    {
        $id = $this->id;
        $this->subscribers[$id] = $aDomainEventSubscriber;
        $this->id++;

        return $id;
    }

    public function ofId($id)
    {
        return isset($this->subscribers[$id]) ? $this->subscribers[$id] : null;
    }

    public function unsubscribe($id)
    {
        unset($this->subscribers[$id]);
    }

    public function publish(Event $aDomainEvent)
    {
        foreach ($this->subscribers as $aSubscriber) {
            if ($aSubscriber->isSubscribedTo($aDomainEvent)) {
                $aSubscriber->handle($aDomainEvent);
            }
        }
    }
}
