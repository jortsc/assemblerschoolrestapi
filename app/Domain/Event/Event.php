<?php
namespace DDDByAssemblerSchool\Domain\Event;

use DDDByAssemblerSchool\Domain\Contact;

/**
 * @author: Jose Manuel Orts
 * @date: 09/05/2020
 */
interface Event
{
    /**
     * @return String
     */
    public function occurredOn(): string;
    /**
     * @return Contact
     */
    public function getEventObject(): Contact;
}
