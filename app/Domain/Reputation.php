<?php
namespace DDDByAssemblerSchool\Domain;

/**
 * @author: Jose Manuel Orts
 * @date: 09/05/2020
 */
interface Reputation
{

    /**
     * @param Contact $contact
     */
    public function get(Contact $contact): int;
}
