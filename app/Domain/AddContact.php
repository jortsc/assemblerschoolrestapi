<?php
namespace DDDByAssemblerSchool\Domain;

use DDDByAssemblerSchool\Application\Service\AddContactServiceRequest;

/**
 * @author: Jose Manuel Orts
 * @date: 09/05/2020
 */
class AddContact
{
    public function process(AddContactServiceRequest $request, Coordinates $coordinates, Reputation $reputation): Contact
    {
        $contact = Contact::createFromRequest($request);

        $contact->setCoordinates($coordinates->get(
            $contact->getStreet(),
            $contact->getPostalCode(),
            $contact->getCity(),
            $contact->getCountry()
        ));

        $contact->setReputation(
            $reputation->get($contact)
        );

        return $contact;
    }
}
