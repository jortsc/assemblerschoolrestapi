<?php
namespace User\V1\Rest\Contact;

use DDDByAssemblerSchool\Application\Service\AddContactService;
use DDDByAssemblerSchool\Infrastructure\GoogleCoordinates;
use DDDByAssemblerSchool\Infrastructure\Persistence\Repository\InMemoryContactRepository;
use DDDByAssemblerSchool\Infrastructure\ReputationApi;

class ContactResourceFactory
{
    public function __invoke($services)
    {
        $service = new AddContactService(new InMemoryContactRepository(), new GoogleCoordinates(), new ReputationApi());
        return new ContactResource($service);
    }
}
