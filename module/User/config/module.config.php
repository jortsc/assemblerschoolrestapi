<?php
return [
    'service_manager' => [
        'factories' => [
            \User\V1\Rest\Contact\ContactResource::class => \User\V1\Rest\Contact\ContactResourceFactory::class,
        ],
    ],
    'router' => [
        'routes' => [
            'user.rest.contact' => [
                'type' => 'Segment',
                'options' => [
                    'route' => '/contact[/:contact_id]',
                    'defaults' => [
                        'controller' => 'User\\V1\\Rest\\Contact\\Controller',
                    ],
                ],
            ],
        ],
    ],
    'api-tools-versioning' => [
        'uri' => [
            0 => 'user.rest.contact',
        ],
    ],
    'api-tools-rest' => [
        'User\\V1\\Rest\\Contact\\Controller' => [
            'listener' => \User\V1\Rest\Contact\ContactResource::class,
            'route_name' => 'user.rest.contact',
            'route_identifier_name' => 'contact_id',
            'collection_name' => 'contact',
            'entity_http_methods' => [
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ],
            'collection_http_methods' => [
                0 => 'GET',
                1 => 'POST',
            ],
            'collection_query_whitelist' => [],
            'page_size' => 25,
            'page_size_param' => null,
            'entity_class' => \User\V1\Rest\Contact\ContactEntity::class,
            'collection_class' => \User\V1\Rest\Contact\ContactCollection::class,
            'service_name' => 'contact',
        ],
    ],
    'api-tools-content-negotiation' => [
        'controllers' => [
            'User\\V1\\Rest\\Contact\\Controller' => 'HalJson',
        ],
        'accept_whitelist' => [
            'User\\V1\\Rest\\Contact\\Controller' => [
                0 => 'application/vnd.user.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ],
        ],
        'content_type_whitelist' => [
            'User\\V1\\Rest\\Contact\\Controller' => [
                0 => 'application/vnd.user.v1+json',
                1 => 'application/json',
            ],
        ],
    ],
    'api-tools-hal' => [
        'metadata_map' => [
            \User\V1\Rest\Contact\ContactEntity::class => [
                'entity_identifier_name' => 'id',
                'route_name' => 'user.rest.contact',
                'route_identifier_name' => 'contact_id',
                'hydrator' => \Laminas\Hydrator\ArraySerializable::class,
            ],
            \User\V1\Rest\Contact\ContactCollection::class => [
                'entity_identifier_name' => 'id',
                'route_name' => 'user.rest.contact',
                'route_identifier_name' => 'contact_id',
                'is_collection' => true,
            ],
        ],
    ],
];
